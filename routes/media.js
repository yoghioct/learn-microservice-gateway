var express = require('express');
var router = express.Router();

const mediaHandler = require('./handler/media')

/* GET users listing. */
router.post('/', mediaHandler.create);

module.exports = router;